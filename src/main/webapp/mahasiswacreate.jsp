<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<title>Insert Mahasiswa</title>
</head>
<body>
<div class="container"></br></br>
    <form action="mahasiswacreate" method="post">
      <div class="form-group">
        <label for="exampleInputEmail1">ID Matkul</label>
        <input type="number" class="form-control" name="id_matkul">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Nama</label>
        <input type="text" class="form-control" name="nama">
      </div>
      <div class="form-group">
              <label for="exampleInputPassword1">Jurusan</label>
              <input type="text" class="form-control" name="jurusan">
            </div>
            <div class="form-group">
                    <label for="exampleInputPassword1">Semester</label>
                    <input type="number" class="form-control" name="semester">
                  </div>
                  <div class="form-group">
                          <label for="exampleInputPassword1">Fakultas</label>
                          <input type="text" class="form-control" name="fakultas">
                        </div>
                        <div class="form-group">
                                <label for="exampleInputPassword1">Alamat</label>
                                <input type="text" class="form-control" name="alamat">
                              </div>
      <button type="submit" class="btn btn-primary" value="Save">Save</button>
    <a href="mahasiswa" type="submit" class="btn btn-success">Back</a>
    </form>
    </div>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>
