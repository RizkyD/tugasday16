<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<title>Home</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="row justify-content-center">
    <div class="card col-sm-3" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">Add Mahasiswa</h5>
        <p class="card-text">Menambah Mahasiswa Baru</p>
        <a href="mahasiswa" class="btn btn-primary">Submit</a>
      </div>
    </div>

    <div class="card col-sm-3" style="width: 18rem;">
          <div class="card-body">
            <h5 class="card-title">Add Pengajar</h5>
            <p class="card-text">Menambah Penagajar Baru</p>
            <a href="pengajar" class="btn btn-primary">Submit</a>
          </div>
        </div>

        <div class="card col-sm-3" style="width: 18rem;">
              <div class="card-body">
                <h5 class="card-title">Add Mata Pelajaran</h5>
                <p class="card-text">Menambah Mata Pelajaran</p>
                <a href="matakuliah" class="btn btn-primary">Submit</a>
              </div>
            </div>

            <div class="card col-sm-3" style="width: 18rem;">
                  <div class="card-body">
                    <h5 class="card-title">Table Keseluruhan</h5>
                    <p class="card-text">Table Join</p>
                    <a href="join" class="btn btn-primary">Submit</a>
                  </div>
                </div>

                </div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
