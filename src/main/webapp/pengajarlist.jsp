<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<title>Data Pengajar</title>
</head>
<body>
<div class="container"></br></br>
    <h1>Pengajar Data</h1>
            <a href="home" type="button" class="btn btn-danger">Home</a>
    <a href="pengajarcreate" type="button" class="btn btn-success">Add Data</a>
    <table class="table table-hover">
          <thead>
            <tr>
                   <th scope="col">id</th>
                   <th scope="col">nama</th>
                   <th scope="col">id_matkul</th>
                   <th scope="col">tahun_bergabung</th>
                   <th scope="col">jadwal</th>
                   <th scope="col">action</th>

            </tr>
          </thead>
          <tbody>
            <c:forEach var="element" items="${requestScope['data']}">
                        <tr>
                            <td>${element.id}</td>
                            <td>${element.nama}</td>
                            <td>${element.id_matkul}</td>
                            <td>${element.tahun_bergabung}</td>
                            <td>${element.jadwal}</td>
                            <td>
                                <a href="pengajaredit?id=${element.id}" type="button" class="btn btn-primary">Edit</a>
                                <a href="pengajardelete?id=${element.id}" type="button" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                     </c:forEach>
          </tbody>
        </table>

</body>
</html>
