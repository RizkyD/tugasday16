<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<title>Data Mata Kuliah</title>
</head>
<body>
<div class="container"></br></br>
    <h1>Mata Kuliah Data</h1></br>
            <a href="home" type="button" class="btn btn-danger">Home</a>
    <a href="matakuliahcreate" type="button" class="btn btn-success">Add Data</a>

    <table class="table table-hover">
      <thead>
        <tr>
               <th scope="col">id</th>
               <th scope="col">nama</th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="element" items="${requestScope['data']}">
                    <tr>
                        <td>${element.id}</td>
                        <td>${element.nama}</td>
                        <td>
                            <a href="matakuliahedit?id=${element.id}" type="button" class="btn btn-primary">Edit</a>
                            <a href="matakuliahdelete?id=${element.id}" type="button" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                 </c:forEach>
      </tbody>
    </table>
    </div>

    	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>
