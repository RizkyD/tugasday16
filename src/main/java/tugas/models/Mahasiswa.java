package tugas.models;

public class Mahasiswa {
    int id, id_matkul, nilai;
    String nama, jurusan, semester, fakultas, alamat;

    public Mahasiswa(int id, int id_matkul, String nama, String jurusan, String semester, String fakultas, String alamat, int nilai) {
        this.id = id;
        this.id_matkul = id_matkul;
        this.nama = nama;
        this.jurusan = jurusan;
        this.semester = semester;
        this.fakultas = fakultas;
        this.alamat = alamat;
        this.nilai = nilai;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_matkul() {
        return id_matkul;
    }

    public void setId_matkul(int id_matkul) {
        this.id_matkul = id_matkul;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getFakultas() {
        return fakultas;
    }

    public void setFakultas(String fakultas) {
        this.fakultas = fakultas;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getNilai() {
        return nilai;
    }

    public void setNilai(int nilai) {
        this.nilai = nilai;
    }
}