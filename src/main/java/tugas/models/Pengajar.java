package tugas.models;

public class Pengajar {
    int id, id_matkul, tahun_bergabung;
    String nama, jadwal;

    public Pengajar(int id, String nama, int id_matkul, int tahun_bergabung, String jadwal) {
        this.id = id;
        this.nama = nama;
        this.id_matkul = id_matkul;
        this.tahun_bergabung = tahun_bergabung;
        this.jadwal = jadwal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getId_matkul() {
        return id_matkul;
    }

    public void setId_matkul(int id_matkul) {
        this.id_matkul = id_matkul;
    }

    public int getTahun_bergabung() {
        return tahun_bergabung;
    }

    public void setTahun_bergabung(int tahun_bergabung) {
        this.tahun_bergabung = tahun_bergabung;
    }

    public String getJadwal() {
        return jadwal;
    }

    public void setJadwal(String jadwal) {
        this.jadwal = jadwal;
    }
}
