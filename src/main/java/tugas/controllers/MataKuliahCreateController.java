package tugas.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tugas.models.Database;

public class MataKuliahCreateController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("/matkulcreate.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Database conn = new Database();
        Connection c = conn.Database();
        String nama = req.getParameter("nama");

        try {
            Statement state = c.createStatement();
            state.execute("INSERT INTO mata_kuliah(nama) VALUES('"+nama+"')");
            resp.sendRedirect("matakuliah");
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
