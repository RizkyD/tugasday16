package tugas.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.nimbus.State;
import tugas.models.Database;

public class MahasiswaCreateController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("/mahasiswacreate.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Database conn = new Database();
        Connection c = conn.Database();
        int id_matkul = Integer.parseInt(req.getParameter("id_matkul"));
        String nama = req.getParameter("nama");
        String jurusan = req.getParameter("jurusan");
        int semester = Integer.parseInt(req.getParameter("semester"));
        String fakultas = req.getParameter("fakultas");
        String alamat = req.getParameter("alamat");

        try {
            Statement state = c.createStatement();
            state.execute("INSERT INTO mahasiswa(id_matkul,nama,jurusan,semester,fakultas,alamat) VALUES('"+id_matkul+"','"+nama+"','"+jurusan+"','"+semester+"','"+fakultas+"','"+alamat+"')");
            resp.sendRedirect("mahasiswa");
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
