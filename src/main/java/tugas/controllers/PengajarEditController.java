package tugas.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tugas.models.Database;
import tugas.models.Pengajar;

public class PengajarEditController extends HttpServlet {
    Database conn = new Database();
    Connection c = conn.Database();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Statement state = c.createStatement();
            String id = req.getParameter("id");
            PrintWriter writer = resp.getWriter();
            ResultSet result = state.executeQuery("SELECT * FROM pengajar WHERE id='"+id+"'");
            result.next();
            Pengajar pengajar = new Pengajar(result.getInt("id"), result.getString("nama"), result.getInt("id_matkul"), result.getInt("tahun_bergabung"), result.getString("jadwal"));
            RequestDispatcher rd = req.getRequestDispatcher("/pengajaredit.jsp");
            req.setAttribute("data", pengajar);
            rd.forward(req, resp);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String nama = req.getParameter("nama");
        int id_matkul = Integer.parseInt(req.getParameter("id_matkul"));
        int tahun_bergabung = Integer.parseInt(req.getParameter("tahun_bergabung"));
        String jadwal = req.getParameter("jadwal");
        try {
            Statement state = c.createStatement();
            state.execute("UPDATE pengajar SET nama='"+nama+"',id_matkul='"+id_matkul+"',tahun_bergabung='"+tahun_bergabung+"',jadwal='"+jadwal+"' WHERE id='"+id+"'");
            resp.sendRedirect("pengajar");
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
