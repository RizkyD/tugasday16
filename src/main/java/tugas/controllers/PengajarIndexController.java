package tugas.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tugas.models.Database;
import tugas.models.Pengajar;

public class PengajarIndexController extends HttpServlet   {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Database conn = new Database();
        Connection c = conn.Database();
        RequestDispatcher rd = req.getRequestDispatcher("/pengajarlist.jsp");
        try {
            Statement state = c.createStatement();
            ResultSet result = state.executeQuery("SELECT * FROM pengajar");
            ArrayList<Pengajar> p = new ArrayList<Pengajar>();
            while (result.next()) {
                Pengajar pengajar = new Pengajar(result.getInt("id"), result.getString("nama"), result.getInt("id_matkul"), result.getInt("tahun_bergabung"), result.getString("jadwal"));
                p.add(pengajar);
            }
            req.setAttribute("data", p);
        } catch (Exception e) {
            e.getStackTrace();
        }
        rd.forward(req, resp);
    }
}
