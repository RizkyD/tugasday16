package tugas.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.nimbus.State;
import tugas.models.Database;

public class PengajarCreateController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("/pengajarcreate.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Database conn = new Database();
        Connection c = conn.Database();
        String nama = req.getParameter("nama");
        int id_matkul = Integer.parseInt(req.getParameter("id_matkul"));
        int tahun_bergabung = Integer.parseInt(req.getParameter("tahun_bergabung"));
        String jadwal = req.getParameter("jadwal");

        try {
            Statement state = c.createStatement();
            state.execute("INSERT INTO pengajar(nama,id_matkul,tahun_bergabung,jadwal) VALUES('"+nama+"','"+id_matkul+"','"+tahun_bergabung+"','"+jadwal+"')");
            resp.sendRedirect("pengajar");
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
