package tugas.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tugas.models.Database;
import tugas.models.MataKuliah;

public class MataKuliahIndexController extends HttpServlet   {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Database conn = new Database();
        Connection c = conn.Database();
        RequestDispatcher rd = req.getRequestDispatcher("/matkullist.jsp");
        try {
            Statement state = c.createStatement();
            ResultSet result = state.executeQuery("SELECT * FROM mata_kuliah");
            ArrayList<MataKuliah> mkl = new ArrayList<MataKuliah>();
            while (result.next()) {
                MataKuliah matkul = new MataKuliah(result.getInt("id"), result.getString("nama"));
                mkl.add(matkul);
            }
            req.setAttribute("data", mkl);
        } catch (Exception e) {
            e.getStackTrace();
        }
        rd.forward(req, resp);
    }
}
