package tugas.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tugas.models.Database;
import tugas.models.Mahasiswa;

public class MahasiswaIndexController extends HttpServlet   {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Database conn = new Database();
        Connection c = conn.Database();
        RequestDispatcher rd = req.getRequestDispatcher("/mahasiswalist.jsp");
        try {
            Statement state = c.createStatement();
            ResultSet result = state.executeQuery("SELECT * FROM mahasiswa");
            ArrayList<Mahasiswa> mhs = new ArrayList<Mahasiswa>();
            while (result.next()) {
                Mahasiswa mahasiswa = new Mahasiswa(result.getInt("id"), result.getInt("id_matkul"), result.getString("nama"), result.getString("jurusan"), result.getString("semester"), result.getString("fakultas"), result.getString("alamat"), result.getInt("nilai"));
                mhs.add(mahasiswa);
            }
            req.setAttribute("data", mhs);
        } catch (Exception e) {
            e.getStackTrace();
        }
        rd.forward(req, resp);
    }
}
