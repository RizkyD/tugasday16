package tugas.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tugas.models.Database;
import tugas.models.Mahasiswa;

public class MahasiswaEditController extends HttpServlet {
    Database conn = new Database();
    Connection c = conn.Database();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Statement state = c.createStatement();
            String id = req.getParameter("id");
            PrintWriter writer = resp.getWriter();
            ResultSet result = state.executeQuery("SELECT * FROM mahasiswa WHERE id='"+id+"'");
            result.next();
            Mahasiswa mahasiswa = new Mahasiswa(result.getInt("id"), result.getInt("id_matkul"), result.getString("nama"), result.getString("jurusan"), result.getString("semester"), result.getString("fakultas"), result.getString("alamat"), result.getInt("nilai"));
            RequestDispatcher rd = req.getRequestDispatcher("/mahasiswaedit.jsp");
            req.setAttribute("data", mahasiswa);
            rd.forward(req, resp);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        int id_matkul = Integer.parseInt(req.getParameter("id_matkul"));
        String nama = req.getParameter("nama");
        String jurusan = req.getParameter("jurusan");
        int semester = Integer.parseInt(req.getParameter("semester"));
        String fakultas = req.getParameter("fakultas");
        String alamat = req.getParameter("alamat");
        try {
            Statement state = c.createStatement();
            state.execute("UPDATE mahasiswa SET id_matkul='"+id_matkul+"',nama='"+nama+"',jurusan='"+jurusan+"',semester='"+semester+"',fakultas='"+fakultas+"', alamat='"+alamat+"' WHERE id='"+id+"'");
            resp.sendRedirect("mahasiswa");
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
