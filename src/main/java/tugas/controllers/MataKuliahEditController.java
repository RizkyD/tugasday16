package tugas.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tugas.models.Database;
import tugas.models.MataKuliah;

public class MataKuliahEditController extends HttpServlet {
    Database conn = new Database();
    Connection c = conn.Database();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Statement state = c.createStatement();
            String id = req.getParameter("id");
            PrintWriter writer = resp.getWriter();
            ResultSet result = state.executeQuery("SELECT * FROM mata_kuliah WHERE id='"+id+"'");
            result.next();
            MataKuliah matkul = new MataKuliah(result.getInt("id"), result.getString("nama"));
            RequestDispatcher rd = req.getRequestDispatcher("/matkuledit.jsp");
            req.setAttribute("data", matkul);
            rd.forward(req, resp);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String nama = req.getParameter("nama");
        try {
            Statement state = c.createStatement();
            state.execute("UPDATE mata_kuliah SET nama='"+nama+"' WHERE id='"+id+"'");
            resp.sendRedirect("matakuliah");
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
