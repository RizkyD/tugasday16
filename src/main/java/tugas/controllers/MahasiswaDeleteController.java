package tugas.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tugas.models.Database;

public class MahasiswaDeleteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Database conn = new Database();
        Connection c = conn.Database();
        int id = Integer.parseInt(req.getParameter("id"));
        try {
            Statement statement = c.createStatement();
            statement.execute("DELETE FROM mahasiswa WHERE id='"+id+"'");
            resp.sendRedirect("mahasiswa");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
