package tugas.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

public class JoinController extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try{
            String url = "jdbc:mysql://localhost:3306/rizkydr?user=rizkydr&password=101201";
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Connection con = DriverManager.getConnection(url);
            Statement stm = con.createStatement();
            ResultSet resultSet = stm.executeQuery("SELECT mahasiswa.nama as nama_mahasiswa, pengajar.nama as nama_pengajar, mata_kuliah.nama as nama_matkul, mahasiswa.nilai as nilai_mahasiswa, pengajar.jadwal AS jadwal FROM mahasiswa INNER JOIN mata_kuliah ON mahasiswa.id_matkul = mata_kuliah.id INNER JOIN pengajar ON pengajar.id_matkul = mata_kuliah.id");
            request.setAttribute("val", resultSet);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("join.jsp");
            requestDispatcher.forward(request, response);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
