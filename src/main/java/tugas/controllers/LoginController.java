package tugas.controllers;

import tugas.models.Authenticator;
import tugas.models.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public LoginController(){
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String email = request.getParameter("username");
        String password = request.getParameter("password");
        RequestDispatcher rd = null;

        Authenticator authenticator = new Authenticator();
        String result = authenticator.authenticcate(email, password);
        System.out.println(result);
        if(result.equals("success")){
            rd = request.getRequestDispatcher("/home.jsp");
            User user = new User(email, password);
            request.setAttribute("user", user);
        } else {
            rd = request.getRequestDispatcher("/login.jsp");
        }
        rd.forward(request, response);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("/login.jsp");
        rd.forward(req, res);
    }
}
